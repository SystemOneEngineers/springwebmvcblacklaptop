package com.example.demo;

import java.util.Date;

public class Position {
long Id;
Date reportDateTime;
Date positionDate;
String reportStatus;
String postionHolderLEI;
String isinETDContract;
public Position(long id, Date reportDateTime, Date positionDate, String reportStatus, String postionHolderLEI,
		String isinETDContract) {
	super();
	Id = id;
	this.reportDateTime = reportDateTime;
	this.positionDate = positionDate;
	this.reportStatus = reportStatus;
	this.postionHolderLEI = postionHolderLEI;
	this.isinETDContract = isinETDContract;
}
public long getId() {
	return Id;
}
public void setId(long id) {
	Id = id;
}
public Date getReportDateTime() {
	return reportDateTime;
}
public void setReportDateTime(Date reportDateTime) {
	this.reportDateTime = reportDateTime;
}
public Date getPositionDate() {
	return positionDate;
}
public void setPositionDate(Date positionDate) {
	this.positionDate = positionDate;
}
public String getReportStatus() {
	return reportStatus;
}
public void setReportStatus(String reportStatus) {
	this.reportStatus = reportStatus;
}
public String getPostionHolderLEI() {
	return postionHolderLEI;
}
public void setPostionHolderLEI(String postionHolderLEI) {
	this.postionHolderLEI = postionHolderLEI;
}
public String getIsinETDContract() {
	return isinETDContract;
}
public void setIsinETDContract(String isinETDContract) {
	this.isinETDContract = isinETDContract;
}
@Override
public String toString() {
	return "Position [Id=" + Id + ", reportDateTime=" + reportDateTime + ", positionDate=" + positionDate
			+ ", reportStatus=" + reportStatus + ", postionHolderLEI=" + postionHolderLEI + ", isinETDContract="
			+ isinETDContract + ", getId()=" + getId() + ", getReportDateTime()=" + getReportDateTime()
			+ ", getPositionDate()=" + getPositionDate() + ", getReportStatus()=" + getReportStatus()
			+ ", getPostionHolderLEI()=" + getPostionHolderLEI() + ", getIsinETDContract()=" + getIsinETDContract()
			+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
}



}
