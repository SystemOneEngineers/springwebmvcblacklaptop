package com.example.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

@GetMapping ("/books") 
public List<Book> getAllBooks() {
	//return Arrays.asList( new Book(11,"Master Shredd","Mohamed"));
	return Arrays.asList( new Book(11,"Master Shredd","Mohamed"),new Book(121,"Master Shredxyzd","Mohamedxyz") );
}
@GetMapping ("/book") 
public List<Book> getOneBooks() {
	//return Arrays.asList( new Book(11,"Master Shredd","Mohamed"));
	return Arrays.asList( new Book(11111,"Michaael","One"));
}
public enum positionStatus {
NEWT,
CANC,
AMND;
	
public static positionStatus getRandom(){
	Random random  = new Random();
	return values()[random.nextInt(values().length)];
}
}/**
public static <T extends Enum<positionStatus>> T randomEnum(Class<positionStatus> clazz) {
	Random random = new Random();
	int x = random.nextInt(clazz.getEnumConstants().length);
	return clazz.getEnumConstants()[x];
}**/


@GetMapping("/randpositions")
public List<Position> getRandomPositions(){
	Random r = new Random();
	int irannum=r.nextInt ( 50-10);
	long ms;
	ms= -946771200000L+ (Math.abs(r.nextLong())%(70L*365*24*60*60*1000));
	Date dt = new Date(ms);
	
	List<Position> lp = new ArrayList<>(); 
	
	for (int i=0; i <= irannum; i++){
		Position pos = new Position(i,dt,dt,positionStatus.getRandom().name(),RandomStringUtils.randomAlphanumeric(16),RandomStringUtils.randomAlphanumeric(16));
		lp.add(pos);
	}
	//return Arrays .asList(lp);
	return lp;
}
}
